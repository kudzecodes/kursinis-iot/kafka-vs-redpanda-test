#!/bin/bash

sudo rm -rf docker/volumes
mkdir -p docker/volumes/kafka docker/volumes/zookeeper docker/volumes/redpanda
sudo chown -R 1001:1001 docker/volumes/kafka
sudo chown -R 1001:1001 docker/volumes/zookeeper
sudo chown -R 101:101 docker/volumes/redpanda