# Kafka vs Redpanda speed performance test.

We used this test to define wherether its best to use kafka or redpanda.

## Tests

### Producer test:

```
./kafka-producer-perf-test.sh --topic test --num-records 10000000 --throughput -1 --record-size 1024 --producer-props bootstrap.servers=localhost:9093
```

### Consumer test:

```
./kafka-consumer-perf-test.sh --bootstrap-server localhost:9093 --topic test -messages 10000000
```

## Running

### Kafka

1. Execute `./prepare-volumes.sh`
2. Execute `docker compose -f docker-compose.kafka.yml up --build`

### Redpanda

1. Execute `./prepare-volumes.sh`
2. Execute `docker compose -f docker-compose.redpanda.yml up --build`

## Test results

### Kafka

#### Producer

```
670381 records sent, 134076.2 records/sec (130.93 MB/sec), 215.5 ms avg latency, 586.0 ms max latency.
1006740 records sent, 201348.0 records/sec (196.63 MB/sec), 152.6 ms avg latency, 176.0 ms max latency.
1036785 records sent, 207357.0 records/sec (202.50 MB/sec), 148.1 ms avg latency, 167.0 ms max latency.
1044420 records sent, 208884.0 records/sec (203.99 MB/sec), 147.2 ms avg latency, 161.0 ms max latency.
1014315 records sent, 202863.0 records/sec (198.11 MB/sec), 151.2 ms avg latency, 188.0 ms max latency.
1035435 records sent, 207087.0 records/sec (202.23 MB/sec), 148.2 ms avg latency, 167.0 ms max latency.
1031310 records sent, 206262.0 records/sec (201.43 MB/sec), 149.1 ms avg latency, 169.0 ms max latency.
992310 records sent, 198462.0 records/sec (193.81 MB/sec), 154.1 ms avg latency, 402.0 ms max latency.
1057365 records sent, 211473.0 records/sec (206.52 MB/sec), 145.7 ms avg latency, 300.0 ms max latency.
1031475 records sent, 206295.0 records/sec (201.46 MB/sec), 149.1 ms avg latency, 167.0 ms max latency.
10000000 records sent, 198499.344952 records/sec (193.85 MB/sec), 153.82 ms avg latency, 586.00 ms max latency, 149 ms 50th, 169 ms 95th, 306 ms 99th, 514 ms 99.9th.
```

#### Consumer

```
start.time, end.time, data.consumed.in.MB, MB.sec, data.consumed.in.nMsg, nMsg.sec, rebalance.time.ms, fetch.time.ms, fetch.MB.sec, fetch.nMsg.sec
2023-10-22 07:44:42:271, 2023-10-22 07:44:55:480, 9765.6250, 739.3160, 10000000, 757059.5806, 3473, 9736, 1003.0428, 1027115.8587
```

### Redpanda

#### Producer

```
952891 records sent, 190578.2 records/sec (186.11 MB/sec), 148.0 ms avg latency, 551.0 ms max latency.
1163985 records sent, 232797.0 records/sec (227.34 MB/sec), 131.9 ms avg latency, 154.0 ms max latency.
1152075 records sent, 230415.0 records/sec (225.01 MB/sec), 133.2 ms avg latency, 163.0 ms max latency.
1217970 records sent, 243594.0 records/sec (237.88 MB/sec), 126.2 ms avg latency, 150.0 ms max latency.
1173180 records sent, 234636.0 records/sec (229.14 MB/sec), 130.5 ms avg latency, 151.0 ms max latency.
1198320 records sent, 239664.0 records/sec (234.05 MB/sec), 127.8 ms avg latency, 150.0 ms max latency.
1186770 records sent, 237354.0 records/sec (231.79 MB/sec), 129.1 ms avg latency, 162.0 ms max latency.
1167090 records sent, 233418.0 records/sec (227.95 MB/sec), 131.4 ms avg latency, 159.0 ms max latency.
10000000 records sent, 230723.086152 records/sec (225.32 MB/sec), 131.69 ms avg latency, 551.00 ms max latency, 129 ms 50th, 147 ms 95th, 172 ms 99th, 366 ms 99.9th.
```

#### Consumer

```
start.time, end.time, data.consumed.in.MB, MB.sec, data.consumed.in.nMsg, nMsg.sec, rebalance.time.ms, fetch.time.ms, fetch.MB.sec, fetch.nMsg.sec
2023-10-22 08:06:43:647, 2023-10-22 08:06:52:408, 9765.6250, 1114.6701, 10000000, 1141422.2121, 326, 8435, 1157.7504, 1185536.4552
```

## Conclusion

Redpanda seems to be faster. Lets use redpanda instead of kafka.